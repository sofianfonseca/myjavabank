package org.academiadecodigo.javabank.services.dao;

import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.services.jpa.JpaSessionManager;
import java.util.List;

public class JpaCustomerDao implements CustomerDao{

    JpaSessionManager jsm;

    @Override
    public List<Customer> findAll() {
        return null;
    }

    @Override
    public Customer findById(Integer id) {
        return null;
    }

    @Override
    public Customer saveOrUpdate(Customer customer) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Customer findByUserName(String username) {
        return null;
    }

    @Override
    public Customer findByEmail(String email) {
        return null;
    }
}
