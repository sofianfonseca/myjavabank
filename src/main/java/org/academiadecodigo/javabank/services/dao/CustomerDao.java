package org.academiadecodigo.javabank.services.dao;
import org.academiadecodigo.javabank.model.Customer;

public interface CustomerDao extends Dao<Customer> {

    Customer findByUserName(String username);
    Customer findByEmail(String email);
}
