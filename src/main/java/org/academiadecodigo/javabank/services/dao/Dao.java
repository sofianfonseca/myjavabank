package org.academiadecodigo.javabank.services.dao;
import org.academiadecodigo.javabank.model.Customer;

import java.util.List;

public interface Dao <T> {

    List<Customer> findAll();
    Customer findById(Integer id);
    Customer saveOrUpdate(Customer customer);
    void delete(Integer id);
}
