package org.academiadecodigo.javabank.services.jpa;

public class JpaTransactionManager implements TransactionManager{

    @Override
    public void beginRead() {

    }

    @Override
    public void beginWrite() {

    }

    @Override
    public void commit() {

    }

    @Override
    public void rollback() {

    }
}
