package org.academiadecodigo.javabank.services.jpa;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;

public class JpaSessionManager {

    private EntityManagerFactory emf;
    private EntityManager em;

    public void startSession(){

        if (em == null){
            em = emf.createEntityManager();
        }
    }

    public void stopSession(){

        if(em != null){
            em.close();
        }
        em = null;
    }

    public EntityManager getCurrentSession(){
        startSession();
        return em;
    }
}
